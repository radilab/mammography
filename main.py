'''
Created on Jun 19, 2016

@author: Ivan Radosavljevic
'''

import dicom
import sys
import struct
import numpy


def load_data(path_in, path_out):
    
    dcm = dicom.read_file(path_in)
    #print(dcm.Rows)
    #print(dcm.Columns)
    pixels = dcm.pixel_array
    
    lut_min_cutoff = dcm[0x0028,0x3010][0][0x0028,0x3002][1]
    lut_length = dcm[0x0028,0x3010][0][0x0028,0x3002][0]
    lut_values = numpy.asarray(dcm[0x0028,0x3010][0][0x0028,0x3006].value)
    
    lut_min = dcm[0x0028,0x3010][0][0x0028,0x3006][0]
    lut_max = dcm[0x0028,0x3010][0][0x0028,0x3006][lut_length-1]
    
    rows = dcm.Rows
    columns = dcm.Columns
    
    
    
    f = open(path_out, "wb")
    #f.write("P2\n")
    f.write(struct.pack('i', rows))
    f.write(struct.pack('i', columns))
    
    #Lut length
    f.write(struct.pack('i', lut_length))
    #Lut min cutoff
    f.write(struct.pack('i', lut_min_cutoff))
    #Lut values
    lut_values_bstr = lut_values.tostring()
    f.write(lut_values_bstr)
    
    #Pixel data
    pixels_bstr = pixels.tostring()
    f.write(pixels_bstr)
    
    #print("asd")
    #for i in range(rows):
    #    for j in range(columns):
    #        val = pixels[i][j]
    #        print(val)
            #f.write(str(val))
            #if(val < lut_min_cutoff):
            #    pixels[i][j] = lut_min
            #elif(val > lut_min_cutoff + lut_length):
            #    pixels[i][j] = lut_max
            #else:
            #    pixels[i][j] = lut_values[val - lut_min_cutoff - 1]
        
            #pixels[i][j] = (256 / (1 + math.exp(-4 * (( pixels[i][j] - window_center ) / window_width ) )))
                #print(pixels[i][j], i, j)
            
            #if(j < dcm.Columns - 1):
            #    f.write("{0} ".format(pixels[i][j]))
            #else:
            #    f.write("{0}\n".format(pixels[i][j]))
            
    f.close()
    
    #print(lut_min)
    #print(lut_max)
    #print(pixels)

if __name__ == '__main__':
    if(len(sys.argv) != 3):
        print("Error")
    else:
        load_data(sys.argv[1], sys.argv[2])
    #load_data("i16.dcm", "out.bin")
    
