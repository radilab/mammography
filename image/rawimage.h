#ifndef RAW_IMAGE_H
#define RAW_IMAGE_H

#include <iostream>
#include <cstdlib>

//#include <CL/cl.hpp>
#include <glibmm.h>
#include <gtkmm.h>
using namespace std;

class RawImage
{
    private:
        int rows = 0;
        int columns = 0;

        uint8_t *pixelData;

    public:
        RawImage(int rows, int columns);
        RawImage(string path);
        void fromDcm(string path);
        Glib::RefPtr<Gdk::Pixbuf> createPixbuf();

        int getRows() {return rows;};
        int getColumns() {return columns;};
        uint8_t *getPixelData() {return pixelData;};

        virtual ~RawImage();
};

#endif // RAW_IMAGE_H
