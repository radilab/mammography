#include "rawimage.h"

#include <fstream>
#include <cmath>

RawImage::RawImage(int rows, int columns)
{
    this->rows = rows;
    this->columns = columns;

    pixelData = new uint8_t[rows*columns];
}

RawImage::RawImage(string path)
{
    fromDcm(path);
}

RawImage::~RawImage()
{
    delete [] pixelData;
}

void RawImage::fromDcm(string path)
{
    int lutLength = 0;
    int lutMinCutoff = 0;
    int lutMaxCutoff = 0;

    int *lut;
    int16_t *rawPixelData;

    Glib::spawn_command_line_sync(string("python main.py \"") + path + "\" out.pcm");
    FILE *f = fopen("out.pcm", "r+b");

    //Load metadata
    fread(&rows, sizeof(int), 1, f);
    fread(&columns, sizeof(int), 1, f);
    fread(&lutLength, sizeof(int), 1, f);
    fread(&lutMinCutoff, sizeof(int), 1, f);

    //Load LUT
    lut = new int[lutLength];
    fread(lut, sizeof(int), lutLength, f);

    //Load pixelr raw data
    rawPixelData = new int16_t[rows*columns];
    fread(rawPixelData, sizeof(int16_t), rows*columns, f);

    fclose(f);

    lutMaxCutoff = lutMinCutoff + lutLength;

    pixelData = new uint8_t[rows * columns];

    /*std::vector<cl::Platform> allPlatforms;
    cl::Platform::get(&allPlatforms);
    std::vector<cl::Device> devices;
    allPlatforms[0].getDevices(CL_DEVICE_TYPE_ALL, &devices);

    cl::Context context({devices[0]});
    cl::Program::Sources sources;*/
    /*
    (256 / (1 - exp(-4.0 * (( image[get_global_id(0)] - (double)desc[3]) / (double)desc[4] ))));
    (256 / (1 - exp(-4.0 * (( rawImage[get_global_id(0)] - (double)desc[3]) / (double)desc[4] ))));
    */
    /*string code = "void kernel dcmToImg(global short *rawImage, global const int *lut, global char *image, global const int *desc)"
    "{"
    "if(rawImage[get_global_id(0)] < desc[1])"
    "{ rawImage[get_global_id(0)] = lut[0]; }"
    "else if(rawImage[get_global_id(0)] > desc[2])"
    "{ rawImage[get_global_id(0)] = lut[desc[0] - 1]; }"
    "else"
    "{ rawImage[get_global_id(0)] = lut[rawImage[get_global_id(0)] - desc[1] - 1]; }"
    "image[get_global_id(0)] = (256.0 / (1.0 - exp(-4.0 * (( (double)rawImage[get_global_id(0)] - desc[3]) / (double)desc[4] ))));"
    "}";

    sources.push_back({code.c_str(), code.length()});

    cl::Program program(context, sources);

    if(program.build({devices[0]}) != CL_SUCCESS)
    {
        cout << "FAIL" << endl;
        cout << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices[0]);
        return;
    }

    cl::Buffer bufferA(context, CL_MEM_READ_WRITE, sizeof(int16_t) * rows * columns);
    cl::Buffer bufferB(context, CL_MEM_READ_WRITE, sizeof(int) * lutLength);
    cl::Buffer bufferC(context, CL_MEM_READ_WRITE, sizeof(int8_t) * rows * columns);
    cl::Buffer bufferD(context, CL_MEM_READ_WRITE, sizeof(int) * 5);

    cl::CommandQueue cmdQueue(context, devices[0]);

    cmdQueue.enqueueWriteBuffer(bufferA, CL_TRUE, 0, sizeof(int16_t) * rows * columns, rawPixelData);
    cmdQueue.enqueueWriteBuffer(bufferB, CL_TRUE, 0, sizeof(int) * lutLength, lut);

    int *desc = new int[5];
    desc[0] = lutLength;
    desc[1] = lutMinCutoff;
    desc[2] = lutMaxCutoff;
    desc[3] = lutMaxCutoff / 2;
    desc[4] = lutMaxCutoff / 2;

    cmdQueue.enqueueWriteBuffer(bufferD, CL_TRUE, 0, sizeof(int) * 5, desc);

    cl::Kernel dcmToImageKernel(program, "dcmToImg");

    dcmToImageKernel.setArg(0, bufferA);
    dcmToImageKernel.setArg(1, bufferB);
    dcmToImageKernel.setArg(2, bufferC);
    dcmToImageKernel.setArg(3, bufferD);

    cmdQueue.enqueueNDRangeKernel(dcmToImageKernel, cl::NullRange, cl::NDRange(rows * columns), cl::NullRange);
    cmdQueue.finish();

    cmdQueue.enqueueReadBuffer(bufferC, CL_TRUE, 0, sizeof(int8_t) * rows * columns, pixelData);*/

    double windowCenter = lut[lutLength - 1]/2;
    //double windowWidth = 14819.51;
    double windowWidth = lut[lutLength - 1]/5;

    //int maxVal = -1;
    for(int i = 0; i < rows * columns; i++)
    {
        /*if(rawPixelData[i] < lutMinCutoff)
        {
            rawPixelData[i] = lut[0];
        }
        else if(rawPixelData[i] >= lutMaxCutoff)
        {
            rawPixelData[i] = lut[lutLength - 1];
            //cout << rawPixelData[i] << endl;
        }
        else
        {
            rawPixelData[i] = lut[rawPixelData[i] - lutMinCutoff];
        }*/

        pixelData[i] = 255 - (256.0 / (1.0 + exp(-4.0 * ((rawPixelData[i] - windowCenter) / windowWidth ) ) ));
        /*if(pixelData[i] > maxVal)
        {
            maxVal = pixelData[i];
        }*/
    }

    //cout << "Max val: " << maxVal - 20 << endl;
    delete [] lut;
    delete [] rawPixelData;
}

Glib::RefPtr<Gdk::Pixbuf> RawImage::createPixbuf()
{
    Glib::RefPtr<Gdk::Pixbuf> image = Gdk::Pixbuf::create(Gdk::Colorspace::COLORSPACE_RGB, true, 8, columns, rows);
    int rstr = image->get_rowstride();
    int nchn = image->get_n_channels();
    guchar *px = image->get_pixels();

    int k = 0;
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < columns; j++)
        {
            guchar *p = px + i *rstr + j * nchn;
            p[0] = (guchar)pixelData[k];
            p[1] = (guchar)pixelData[k];
            p[2] = (guchar)pixelData[k];
            p[3] = 255;
            k++;
        }

    }
    return image;
}
