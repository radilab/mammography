var files =
[
    [ "canvas.cpp", "canvas_8cpp.html", null ],
    [ "canvas.h", "canvas_8h.html", [
      [ "Canvas", "class_canvas.html", "class_canvas" ]
    ] ],
    [ "gui.cpp", "gui_8cpp.html", null ],
    [ "gui.h", "gui_8h.html", [
      [ "MainWindow", "class_main_window.html", "class_main_window" ]
    ] ],
    [ "item.cpp", "item_8cpp.html", null ],
    [ "item.h", "item_8h.html", [
      [ "CanvasItem", "class_canvas_item.html", "class_canvas_item" ]
    ] ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ]
];