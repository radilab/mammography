#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#define _USE_MATH_DEFINES

#include <iostream>
#include <cmath>
#include <gtkmm.h>
#include "../image/rawimage.h"
#include "canvas/canvas.h"

#include "../algorithms/BreastDetector.h"
#include "../algorithms/PectoralMuscleDetector.h"
#include "../algorithms/PectoralMuscleRemover.h"
#include "../algorithms/GlandularTissueDetector.h"
#include "../algorithms/AsymmetryDetector.h"
#include "../algorithms/calcificationdetector.h"

#include "canvas/items/circle.h"
#include "canvas/items/rectangle.h"
#include "canvas/items/polyline.h"
#include "canvas/items/polygon.h"
#include "canvas/items/Image.h"

#include "../image/rawimage.h"

class MainWindow : public Window
{
    private:
        Box *vbox;
        MenuBar *menuBar;
        Toolbar *toolbar;
        HScale *toolbarHs;
        Canvas *leftCanvas;
        Canvas *rightCanvas;

        //Image stuff
        RawImage *leftImage = NULL;
        RawImage *rightImage = NULL;

        void createMenu();
        void createToolbar();
        void loadImages(string path1, string path2);
    protected:
        void on_open();
        void on_process();
        void on_quit();

        void on_slider_chage();
    public:
        MainWindow();
};

#endif //MAIN_WINDOW_H
