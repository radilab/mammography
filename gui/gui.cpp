#include "gui.h"



MainWindow::MainWindow()
{
    set_title("Mammography");
    set_default_geometry(800, 600);
    vbox = manage(new Box(ORIENTATION_VERTICAL, 0));

    ScrolledWindow *swLeft = manage(new ScrolledWindow());
    ScrolledWindow *swRight = manage (new ScrolledWindow());
    HPaned *hp = manage(new HPaned());

    leftCanvas = manage(new Canvas());
    rightCanvas = manage(new Canvas());

    createMenu();
    createToolbar();

    add(*vbox);

    vbox->pack_start(*menuBar, PACK_SHRINK, 0);
    vbox->pack_start(*toolbar, PACK_SHRINK, 0);
    vbox->pack_start(*hp);

    swLeft->add(*leftCanvas);
    swRight->add(*rightCanvas);

    leftCanvas->setScale(toolbarHs->get_value());
    rightCanvas->setScale(toolbarHs->get_value());

    leftCanvas->set_size_request(5000, 6000);
    rightCanvas->set_size_request(5000, 6000);

    hp->add1(*swLeft);
    hp->add2(*swRight);

    vbox->show_all();
}

void MainWindow::createMenu()
{
    menuBar = manage(new MenuBar());

    MenuItem *fileMenuItem = manage(new MenuItem("_Datoteka", true));
    MenuItem *helpMenuItem = manage(new MenuItem("_Pomoć", true));

    MenuItem *openMenuItem = manage(new MenuItem("_Otvori", true));;
    MenuItem *quitMenuItem = manage(new MenuItem("_Izlaz", true));;

    MenuItem *helpHelpMenuItem = manage(new MenuItem("_Pomoć", true));;
    MenuItem *aboutMenuItem = manage(new MenuItem("_O programu", true));;

    Menu *fileMenu = manage(new Menu());
    Menu *helpMenu = manage(new Menu());

    menuBar->append(*fileMenuItem);
    menuBar->append(*helpMenuItem);

    fileMenu->append(*openMenuItem);
    fileMenu->append(*quitMenuItem);
    helpMenu->append(*helpHelpMenuItem);
    helpMenu->append(*aboutMenuItem);

    fileMenuItem->set_submenu(*fileMenu);
    helpMenuItem->set_submenu(*helpMenu);


    openMenuItem->signal_activate().connect(sigc::mem_fun(*this, &MainWindow::on_open));
    quitMenuItem->signal_activate().connect(sigc::mem_fun(*this, &MainWindow::on_quit));
    //menuBar->items.push_back(MenuElem);
}

void MainWindow::createToolbar()
{
    toolbar = manage(new Toolbar());
    ToolButton *openTool = manage(new ToolButton(Stock::OPEN));
    openTool->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::on_open));

    SeparatorToolItem *separator1 = manage(new SeparatorToolItem());

    ToolButton *runTool = manage(new ToolButton(Stock::MEDIA_PLAY));
    runTool->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::on_process));

    ToolItem *scaleItem = manage(new ToolItem());
    toolbarHs = manage(new HScale());
    toolbarHs->set_draw_value(false);
    toolbarHs->set_range(0.1, 1.0);
    toolbarHs->set_size_request(100, -1);
    toolbarHs->set_round_digits(2);
    toolbarHs->signal_value_changed().connect(sigc::mem_fun(*this, &MainWindow::on_slider_chage));
    scaleItem->add(*toolbarHs);

    toolbar->insert(*openTool, -1);
    toolbar->insert(*separator1, -1);
    toolbar->insert(*runTool, -1);
    toolbar->insert(*scaleItem, -1);
}

void MainWindow::on_open()
{
    string path1 = "";
    string path2 = "";

    auto filter = Gtk::FileFilter::create();
    filter->set_name("Dicom slika");
    filter->add_pattern("*.dcm");

    Gtk::FileChooserDialog dialog1("Odaberite levu sliku", Gtk::FileChooserAction::FILE_CHOOSER_ACTION_OPEN);
    dialog1.set_transient_for(*this);
    dialog1.add_filter(filter);

    dialog1.add_button("Otkaži", Gtk::RESPONSE_CANCEL);
    dialog1.add_button("Otvori", Gtk::RESPONSE_OK);

    Gtk::FileChooserDialog dialog2("Odaberite desnu sliku", Gtk::FileChooserAction::FILE_CHOOSER_ACTION_OPEN);
    dialog2.set_transient_for(*this);
    dialog2.add_filter(filter);

    dialog2.add_button("Otkaži", Gtk::RESPONSE_CANCEL);
    dialog2.add_button("Otvori", Gtk::RESPONSE_OK);

    int result = dialog1.run();
    if(result == Gtk::ResponseType::RESPONSE_OK)
    {
        result = dialog2.run();
    }

    if(result == Gtk::ResponseType::RESPONSE_OK)
    {
        path1 = dialog1.get_filename();
        path2 = dialog2.get_filename();

        loadImages(path1, path2);
    }
}

void MainWindow::loadImages(string path1, string path2)
{
    if(leftImage != NULL)
    {
        delete leftImage;
    }
    if(rightImage != NULL)
    {
        delete rightImage;
    }

    leftImage = new RawImage(path1);
    rightImage = new RawImage(path2);


    leftCanvas->addItem(new CanvasImage(leftImage->createPixbuf()));
    rightCanvas->addItem(new CanvasImage(rightImage->createPixbuf()));
}

void MainWindow::on_quit()
{
    this->close();
}

void MainWindow::on_slider_chage()
{
    leftCanvas->setScale(toolbarHs->get_value());
    rightCanvas->setScale(toolbarHs->get_value());
}

void MainWindow::on_process()
{
    BreastDetector bd1(leftImage);
    bd1.detect();

    BreastDetector bd2(rightImage);
    bd2.detect();

    leftCanvas->addItem(new CanvasImage(bd1.createPixbuf()));
    rightCanvas->addItem(new CanvasImage(bd2.createPixbuf()));

    PectoralMuscleDetector pmd1(bd1.getBreast(), true);
    PectoralMuscleDetector pmd2(bd2.getBreast(), false);
    cout << "Pectoral!" << endl;
    pmd1.detect();
    cout << "Pectoral!" << endl;
    pmd2.detect();

    leftCanvas->addItem(new CanvasImage(pmd1.createPixbuf()));
    rightCanvas->addItem(new CanvasImage(pmd2.createPixbuf()));

    double la = cos(pmd1.getTheta() * M_PI/180);
    double lb = sin(pmd1.getTheta() * M_PI/180);
    double lx0 = la * pmd1.getRoh();
    double ly0 = lb * pmd1.getRoh();
    double lx1 = lx0 + 6000 * (-lb);
    double ly1 = ly0 + 6000 * (la);
    double lx2 = lx0 - 6000 * (-lb);
    double ly2 = ly0 - 6000 * (la);

    Polyline *pl1 = new Polyline(lx1, ly1, 10, 0, 1, 1, 1);
    pl1->addPoint(lx2, ly2);
    leftCanvas->addItem(pl1);

    double ra = cos(pmd2.getTheta() * M_PI/180);
    double rb = sin(pmd2.getTheta() * M_PI/180);
    double rx0 = ra * pmd2.getRoh();
    double ry0 = rb * pmd2.getRoh();
    double rx1 = rx0 + 6000 * (-rb);
    double ry1 = ry0 + 6000 * (ra);
    double rx2 = rx0 - 6000 * (-rb);
    double ry2 = ry0 - 6000 * (ra);

    Polyline *pl2 = new Polyline(rx1, ry1, 10, 0, 1, 1, 1);
    pl2->addPoint(rx2, ry2);
    rightCanvas->addItem(pl2);

    PectoralMuscleRemover pmr1(bd1.getBreast(), lx1, ly1, lx2, ly2, true);
    pmr1.detect();

    PectoralMuscleRemover pmr2(bd2.getBreast(), rx1, ry1, rx2, ry2, false);
    pmr2.detect();

    leftCanvas->addItem(new CanvasImage(pmr1.createPixbuf()));
    rightCanvas->addItem(new CanvasImage(pmr2.createPixbuf()));

    GlandularTissueDetector gtd1(pmr1.getBreast());
    gtd1.detect();

    GlandularTissueDetector gtd2(pmr2.getBreast());
    gtd2.detect();

    leftCanvas->addItem(new CanvasImage(gtd1.createPixbuf()));
    rightCanvas->addItem(new CanvasImage(gtd2.createPixbuf()));

    cout << "Zlezdano tkivo (Levi snimak): " << 100 * gtd1.getPercent() << "%" << endl;
    cout << "Zlezdano tkivo (Desni snimak): " << 100 * gtd2.getPercent() << "%" << endl;
    cout << "---------------------------" << endl;
    cout << "Zlezdano tkivo (Levi snimak): " << gtd1.getArea() * 0.05 << "mm^2" << endl;
    cout << "Zlezdano tkivo (Desni snimak): " << gtd2.getArea() * 0.05 << "mm^2" << endl;

    AsymmetryDetector ad(gtd1.getBreast(), gtd2.getBreast());
    ad.detect();

    cout << "Potencijalni tumori (Levi snimak): " << ad.tumorsL.size() << endl;
    cout << "Potencijalni tumori (Desni snimak): " << ad.tumorsR.size() << endl;

    leftCanvas->addItem(new CanvasImage(ad.createPixbufL()));
    rightCanvas->addItem(new CanvasImage(ad.createPixbufR()));

    for(int i = 0; i < ad.tumorsL.size(); i++)
    {
        double minX = ad.tumorsL[i][0]->x;
        double maxX = ad.tumorsL[i][0]->x;
        double minY = ad.tumorsL[i][0]->y;
        double maxY = ad.tumorsL[i][0]->y;
        for(int j = 0; j < ad.tumorsL[i].size(); j++)
        {
            if(ad.tumorsL[i][j]->x > maxX)
            {
                maxX = ad.tumorsL[i][j]->x;
            }

            if(ad.tumorsL[i][j]->y > maxY)
            {
                maxY = ad.tumorsL[i][j]->y;
            }

            if(ad.tumorsL[i][j]->x < minX)
            {
                minX = ad.tumorsL[i][j]->x;
            }

            if(ad.tumorsL[i][j]->y < minY)
            {
                minY = ad.tumorsL[i][j]->y;
            }
        }

        leftCanvas->addItem(new Rectangle(minX, minY, maxX-minX, maxY-minY, 0, 0, 1, 0.5));
    }

    for(int i = 0; i < ad.tumorsR.size(); i++)
    {
        double minX = ad.tumorsR[i][0]->x;
        double maxX = ad.tumorsR[i][0]->x;
        double minY = ad.tumorsR[i][0]->y;
        double maxY = ad.tumorsR[i][0]->y;
        for(int j = 0; j < ad.tumorsR[i].size(); j++)
        {
            if(ad.tumorsR[i][j]->x > maxX)
            {
                maxX = ad.tumorsR[i][j]->x;
            }

            if(ad.tumorsR[i][j]->y > maxY)
            {
                maxY = ad.tumorsR[i][j]->y;
            }

            if(ad.tumorsR[i][j]->x < minX)
            {
                minX = ad.tumorsR[i][j]->x;
            }

            if(ad.tumorsR[i][j]->y < minY)
            {
                minY = ad.tumorsR[i][j]->y;
            }
        }

        rightCanvas->addItem(new Rectangle(minX, minY, maxX-minX, maxY-minY, 0, 0, 1, 0.5));
    }

    /*CalcificationDetector cd1(pmr1.getBreast());
    cd1.detect();
    leftCanvas->addItem(new CanvasImage(cd1.createPixbuf()));*/
}
