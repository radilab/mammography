#include "canvas.h"

using namespace Gtk;

Canvas::Canvas()
{

}

Canvas::~Canvas()
{

}

void Canvas::addItem(CanvasItem* i)
{
    items.push_back(i);
    this->queue_draw();
}

void Canvas::clear()
{
    for(int i = 0; i < items.size(); i++)
    {
        delete items[i];
    }
    items.clear();
}

bool Canvas::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    cr->scale(scale, scale);
    for(int i = 0; i < items.size(); i++)
    {
        items[i]->draw(cr);
    }
    /*Allocation allocation = get_allocation();

    cr->set_source_rgba(0, 0, 0, 1);
    cr->paint();

    const int width = allocation.get_width();
    const int height = allocation.get_height();
    auto img = Gdk::Pixbuf::create_from_file("mango.png");
    Gdk::Cairo::set_source_pixbuf(cr, img, 0, 0);

    cr->rectangle(0, 0, img->get_width(), img->get_height());
    cr->fill();*/
    return true;
}
