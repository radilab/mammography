#ifndef CANVAS_H
#define CANVAS_H

#include <iostream>
#include <gtkmm.h>
#include <vector>

#include "items/item.h"

using namespace Gtk;
using namespace std;

class Canvas : public DrawingArea
{
    private:
        vector<CanvasItem*> items;
    public:
        Canvas();
        virtual ~Canvas();
        void addItem(CanvasItem*);
        void setScale(double scale){this->scale = scale; this->queue_draw();};
        void clear();
    protected:
        double scale = 1;
        bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
};

#endif //CANVAS_H
