#ifndef CIRCLE_H
#define CIRCLE_H

#include "item.h"

class Circle : public CanvasItem
{
    public:
        Circle(double xc = 0, double yc = 0, double r = 0,
               double fillRed = 0, double fillGreen = 0, double fillBlue = 0, double fillAlpha = 1,
               double strokeWidth = 1,
               double strokeRed = 0, double strokeGreen = 0, double strokeBlue = 0, double strokeAlpha = 1
               );
        virtual ~Circle();

        virtual bool draw(const Cairo::RefPtr<Cairo::Context>&);

    protected:

    private:
        double r;


};

#endif // CIRCLE_H
