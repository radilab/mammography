#ifndef POLYGON_H
#define POLYGON_H

#include "item.h"

using namespace std;

class Polygon : public CanvasItem
{
    public:
        Polygon(double x = 0, double y = 0,
                  double fillRed = 0, double fillGreen = 0, double fillBlue = 0, double fillAlpha = 1,
                  double strokeWidth = 1,
                  double strokeRed = 0, double strokeGreen = 0, double strokeBlue = 0, double strokeAlpha = 1);
        virtual ~Polygon();

        virtual bool draw(const Cairo::RefPtr<Cairo::Context>&);

        void addPoint(double x, double y);

    protected:

    private:
        vector<pair<double, double>> points;
};

#endif // POLYGON_H
