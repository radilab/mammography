#include "item.h"

CanvasItem::CanvasItem(double x, double y,
               double fillRed, double fillGreen, double fillBlue, double fillAlpha,
               double strokeWidth,
               double strokeRed, double strokeGreen, double strokeBlue, double strokeAlpha)
{
    this->x = x;
    this->y = y;

    this->fillRed = fillRed;
    this->fillGreen = fillGreen;
    this->fillBlue = fillBlue;
    this->fillAlpha = fillAlpha;

    this->strokeRed = strokeRed;
    this->strokeGreen = strokeGreen;
    this->strokeBlue = strokeBlue;
    this->strokeAlpha = strokeAlpha;

    this->strokeWidth = strokeWidth;
}
