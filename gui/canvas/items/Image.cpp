#include "Image.h"

CanvasImage::CanvasImage(Glib::RefPtr<Gdk::Pixbuf> image, double x, double y) : CanvasItem(x, y)
{
    this->image = image;
}

CanvasImage::~CanvasImage()
{
    //dtor
}

bool CanvasImage::draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    cr->save();
    Gdk::Cairo::set_source_pixbuf(cr, image, 0, 0);
    cr->rectangle(x, x, image->get_width(), image->get_height());
    cr->fill();
    cr->restore();
    /*cr->save();
    cr->set_line_width(strokeWidth);
    cr->arc(x, y, r, 0.0, 2.0 * M_PI); // full circle
    cr->set_source_rgba(fillRed, fillGreen, fillBlue, fillAlpha);    // partially translucent
    cr->fill_preserve();
    cr->set_source_rgba(strokeRed, strokeGreen, strokeBlue, strokeAlpha);
    cr->stroke();
    cr->restore();*/
}
