#include "polygon.h"

Polygon::Polygon(double x , double y ,
                 double fillRed, double fillGreen, double fillBlue, double fillAlpha,
                 double strokeWidth,
                 double strokeRed, double strokeGreen, double strokeBlue, double strokeAlpha
                 ) : CanvasItem(x, y, fillRed, fillGreen, fillBlue, fillAlpha,
                                strokeWidth, strokeRed, strokeGreen, strokeBlue, strokeAlpha)
{
    addPoint(x, y );
}

Polygon::~Polygon()
{
    //dtor
}

void Polygon::addPoint(double x, double y)
{
    points.push_back(pair<double, double> (x, y));
}

bool Polygon::draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    cr->save();
    cr->set_line_width(strokeWidth);

    cr->move_to(points[0].first, points[0].second);

    for(int i = 1; i < points.size(); i++)
    {
        cr->line_to(points[i].first, points[i].second);
    }
    cr->close_path();
    cr->set_source_rgba(fillRed, fillGreen, fillBlue, fillAlpha);
    cr->fill_preserve();
    cr->set_source_rgba(strokeRed, strokeGreen, strokeBlue, strokeAlpha);
    cr->stroke();
    cr->restore();
}
