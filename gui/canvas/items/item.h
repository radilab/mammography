#ifndef CANVAS_ITEM_H
#define CANVAS_ITEM_H

#define _USE_MATH_DEFINES

#include <gtkmm.h>
#include <cmath>

class CanvasItem
{
    public:
        CanvasItem(double x = 0, double y = 0,
                   double fillRed = 0, double fillGreen = 0, double fillBlue = 0, double fillAlpha = 1,
                   double strokeWidth = 1,
                   double strokeRed = 0, double strokeGreen = 0, double strokeBlue = 0, double strokeAlpha = 1);
        virtual bool draw(const Cairo::RefPtr<Cairo::Context>&) = 0;

    protected:
        double x;
        double y;

        double fillRed;
        double fillGreen;
        double fillBlue;
        double fillAlpha;

        double strokeRed;
        double strokeGreen;
        double strokeBlue;
        double strokeAlpha;

        double strokeWidth;
};

#endif // CANVAS_ITEM_H
