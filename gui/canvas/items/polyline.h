#ifndef POLYLINE_H
#define POLYLINE_H

#include <vector>

#include "item.h"

using namespace std;

class Polyline : public CanvasItem
{
    public:
        Polyline(double x = 0, double y = 0, double strokeWidth = 0,
                 double strokeRed = 0, double strokeGreen = 0, double strokeBlue = 0, double strokeAlpha = 1);
        virtual ~Polyline();

        virtual bool draw(const Cairo::RefPtr<Cairo::Context>&);

        void addPoint(double x, double y);

    protected:

    private:
        vector<pair<double, double>> points;

};

#endif // POLYLINE_H
