#ifndef CANVAS_IMAGE_H
#define CANVAS_IMAGE_H

#include "item.h"
#include <gtkmm.h>

class CanvasImage : public CanvasItem
{
    public:
        CanvasImage(Glib::RefPtr<Gdk::Pixbuf> image, double x = 0, double y = 0);
        virtual bool draw(const Cairo::RefPtr<Cairo::Context>&);
        virtual ~CanvasImage();

    protected:
        int width = 0;
        int height = 0;
        Glib::RefPtr<Gdk::Pixbuf> image;
};

#endif // CANVAS_IMAGE_H
