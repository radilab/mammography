#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "item.h"

class Rectangle : public CanvasItem
{
    public:
        Rectangle(double x = 0, double y = 0, double width = 0, double height = 0,
                  double fillRed = 0, double fillGreen = 0, double fillBlue = 0, double fillAlpha = 1,
                  double strokeWidth = 1,
                  double strokeRed = 0, double strokeGreen = 0, double strokeBlue = 0, double strokeAlpha = 1);
        virtual ~Rectangle();

        virtual bool draw(const Cairo::RefPtr<Cairo::Context>&);

    protected:

    private:
        double width;
        double height;

};

#endif // RECTANGLE_H
