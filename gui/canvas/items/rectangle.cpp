#include "rectangle.h"

Rectangle::Rectangle(double x , double y , double width, double height,
                     double fillRed, double fillGreen, double fillBlue, double fillAlpha,
                     double strokeWidth,
                     double strokeRed, double strokeGreen, double strokeBlue, double strokeAlpha
                     ) : CanvasItem(x, y, fillRed, fillGreen, fillBlue, fillAlpha,
                                      strokeWidth, strokeRed, strokeGreen, strokeBlue, strokeAlpha)
{
    this->width = width;
    this->height = height;
}

Rectangle::~Rectangle()
{
    //dtor
}

bool Rectangle::draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    cr->save();
    cr->set_line_width(strokeWidth);
    cr->rectangle(x, y, width, height);
    cr->set_source_rgba(fillRed, fillGreen, fillBlue, fillAlpha);
    cr->fill_preserve();
    cr->set_source_rgba(strokeRed, strokeGreen, strokeBlue, strokeAlpha);
    cr->stroke();
    cr->restore();
}

