#include "circle.h"

Circle::Circle(double xc , double yc , double r,
               double fillRed, double fillGreen, double fillBlue, double fillAlpha,
               double strokeWidth,
               double strokeRed, double strokeGreen, double strokeBlue, double strokeAlpha
               ) : CanvasItem(xc, yc, fillRed, fillGreen, fillBlue, fillAlpha,
                              strokeWidth, strokeRed, strokeGreen, strokeBlue, strokeAlpha)
{
    this->r = r;
}

Circle::~Circle()
{
    //dtor
}

bool Circle::draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    cr->save();
    cr->set_line_width(strokeWidth);
    cr->arc(x, y, r, 0.0, 2.0 * M_PI); // full circle
    cr->set_source_rgba(fillRed, fillGreen, fillBlue, fillAlpha);    // partially translucent
    cr->fill_preserve();
    cr->set_source_rgba(strokeRed, strokeGreen, strokeBlue, strokeAlpha);
    cr->stroke();
    cr->restore();
}
