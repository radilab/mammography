#include "polyline.h"

Polyline::Polyline(double x , double y , double strokeWidth,
                   double strokeRed, double strokeGreen, double strokeBlue, double strokeAlpha
                   ) : CanvasItem(x, y, 0, 0, 0, 0, strokeWidth, strokeRed, strokeGreen, strokeBlue, strokeAlpha)
{
    addPoint(x, y);
}

Polyline::~Polyline()
{
    //dtor
}

void Polyline::addPoint(double x, double y)
{
    points.push_back(pair<double, double> (x, y));
}

bool Polyline::draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    cr->save();
    cr->set_line_width(strokeWidth);

    cr->move_to(points[0].first, points[0].second);

    for(int i = 1; i < points.size(); i++)
    {
        cr->line_to(points[i].first, points[i].second);
    }
    cr->set_source_rgba(strokeRed, strokeGreen, strokeBlue, strokeAlpha);
    cr->stroke();
    cr->restore();
}
