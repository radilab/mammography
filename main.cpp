#include <iostream>
#include <gtkmm.h>


#include "gui/gui.h"

#include "image/rawimage.h"

using namespace std;



/** \brief
 *
 * \param argc int - number of parameters.
 * \param argv char** - array of string parameters.
 * \return int - return code.
 *
 */
int main(int argc, char **argv)
{
    auto app = Gtk::Application::create(argc, argv);

    MainWindow mainWindow;
    app->run(mainWindow);
}
