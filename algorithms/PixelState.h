#ifndef PIXELSTATE_H
#define PIXELSTATE_H

#include <iostream>

class PixelState
{
    public:
        PixelState();
        virtual ~PixelState();
        uint16_t x;
        uint16_t y;
        uint8_t state;

    protected:


    private:
};

#endif // PIXELSTATE_H
