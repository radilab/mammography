#include "calcificationdetector.h"

CalcificationDetector::CalcificationDetector(RawImage *img)
{
    this->inputImage = img;
}

CalcificationDetector::~CalcificationDetector()
{

}

void CalcificationDetector::detect()
{
    cout << "Test" << endl;
    tmpImage = new uint8_t[inputImage->getRows() * inputImage->getColumns()];

    int kernelX[3][3] = {{-1, 0, 1},
                         {-2, 0, 2},
                         {-1, 0, 1}};
    int kernelY[3][3] = {{-1, -2, -1},
                         {0, 0, 0},
                         {1, 2, 1}};

    int kernelCC[11][11] = {{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
    {-1, -1, -1, -1, -1, 0, -1, -1, -1, -1, -1},
    {-1, -1, -1, -1, 0, 1, 0, -1, -1, -1, -1},
    {-1, -1, -1, 0, 1, 2, 1, 0, -1, -1, -1},
    {-1, -1, 0, 1, 2, 3, 2, 1, 0, -1, -1},
    {-1, 0, 1, 2, 3, 3, 3, 2, 1, 0, -1},
    {-1, -1, 0, 1, 2, 3, 2, 1, 0, -1, -1},
    {-1, -1, -1, 0, 1, 2, 1, 0, -1, -1, -1},
    {-1, -1, -1, -1, 0, 1, 0, -1, -1, -1, -1},
    {-1, -1, -1, -1, -1, 0, -1, -1, -1, -1, -1},
    {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}};

    double avg = 0;
    int pixels = 0;
    for(int i = 0; i < inputImage->getRows(); i++)
    {
        for(int j = 0; j < inputImage->getColumns(); j++)
        {
            double sumX = 0;
            double sumY = 0;
            for(int k = -1; k < 2; k++)
            {
                for(int l = -1; l < 2; l++)
                {
                    if(((i + k) >= 0)&&((i + k) < inputImage->getRows())&&((j + l) >= 0)&&((j + l) < inputImage->getColumns()))
                    {
                        sumX += inputImage->getPixelData()[(i+k)*inputImage->getColumns() + j+l] * kernelX[1+k][1+l];
                        sumY += inputImage->getPixelData()[(i+k)*inputImage->getColumns() + j+l] * kernelY[1+k][1+l];
                    }
                }
            }

            tmpImage[i*inputImage->getColumns() + j] = sqrt(sumX*sumX + sumY*sumY);

            if(tmpImage[i*inputImage->getColumns() + j] != 0)
            {
                pixels++;
                avg += tmpImage[i*inputImage->getColumns() + j];
            }
        }
    }

    avg = avg / pixels;
    for(int i = 0; i < inputImage->getRows() * inputImage->getColumns(); i++)
    {
        if(tmpImage[i] <= avg)
        {
            tmpImage[i] = 0;
        }
    }
    for(int i = 0; i < inputImage->getRows(); i++)
    {
        for(int j = 0; j < inputImage->getColumns(); j++)
        {
            double sum = 0;
            for(int k = -5; k < 6; k++)
            {
                for(int l = -5; l < 6; l++)
                {
                    if(((i + k) >= 0)&&((i + k) < inputImage->getRows())&&((j + l) >= 0)&&((j + l) < inputImage->getColumns()))
                    {
                        sum += inputImage->getPixelData()[(i+k)*inputImage->getColumns() + j+l] * kernelCC[5+k][5+l];
                    }
                }
            }

            if(sum > 0)
            {
                tmpImage[i*inputImage->getColumns() + j] = 255;
            }
        }
    }

    int total_pixels = 0;

    uint8_t *tmpImage2 = new uint8_t[inputImage->getColumns() * inputImage->getRows()];

    for(int i = 0; i < inputImage->getRows(); i++)
    {
        for(int j = 0; j < inputImage->getColumns(); j++)
        {
            if(tmpImage[i*inputImage->getColumns() + j] != 0)
            {
                if((i - 1 >= 0)&&(j - 1 >=0)&&(tmpImage[(i-1)*inputImage->getColumns() + (j-1)] == 0))
                {
                    tmpImage2[i*inputImage->getColumns() + j] = 0;
                }
                else if((i + 1 < inputImage->getRows())&&(j - 1 >=0)&&(tmpImage[(i+1)*inputImage->getColumns() + (j-1)] == 0))
                {
                    tmpImage2[i*inputImage->getColumns() + j] = 0;
                }
                else if((i - 1 >= 0)&&(j + 1 < inputImage->getColumns())&&(tmpImage[(i-1)*inputImage->getColumns() + (j+1)] == 0))
                {
                    tmpImage2[i*inputImage->getColumns() + j] = 0;
                }
                else if((i + 1 < inputImage->getRows())&&(j + 1 < inputImage->getColumns())&&(tmpImage[(i+1)*inputImage->getColumns() + (j+1)] == 0))
                {
                    tmpImage2[i*inputImage->getColumns() + j] = 0;
                }
                else if((i - 1 >= 0)&&(tmpImage[(i-1)*inputImage->getColumns() + j] == 0))
                {
                    tmpImage2[i*inputImage->getColumns() + j] = 0;
                }
                else if((i + 1 < inputImage->getRows())&&(tmpImage[(i+1)*inputImage->getColumns() + j] == 0))
                {
                    tmpImage2[i*inputImage->getColumns() + j] = 0;
                }
                else if((j - 1 >= 0)&&(tmpImage[i*inputImage->getColumns() + (j-1)] == 0))
                {
                    tmpImage2[i*inputImage->getColumns() + j] = 0;
                }
                else if((j + 1 < inputImage->getColumns())&&(tmpImage[i*inputImage->getColumns() + (j+1)] == 0))
                {
                    tmpImage2[i*inputImage->getColumns() + j] = 0;
                }
                else
                {
                    tmpImage2[i*inputImage->getColumns() + j] = 255;
                }
            }
        }
    }

    uint8_t *arr = tmpImage;
    tmpImage = tmpImage2;
    delete [] arr;

    uint8_t *tmpImage3 = new uint8_t[inputImage->getColumns() * inputImage->getRows()];

    for(int i = 0; i < inputImage->getRows(); i++)
    {
        for(int j = 0; j < inputImage->getColumns(); j++)
        {
            if(tmpImage[i*inputImage->getColumns() + j] != 0)
            {
                if((i - 1 >= 0)&&(j - 1 >=0)&&(tmpImage[(i-1)*inputImage->getColumns() + (j-1)] != 0))
                {
                    tmpImage3[i*inputImage->getColumns() + j] = 255;
                }
                else if((i + 1 < inputImage->getRows())&&(j - 1 >=0)&&(tmpImage[(i+1)*inputImage->getColumns() + (j-1)] != 0))
                {
                    tmpImage3[i*inputImage->getColumns() + j] = 255;
                }
                else if((i - 1 >= 0)&&(j + 1 < inputImage->getColumns())&&(tmpImage[(i-1)*inputImage->getColumns() + (j+1)] != 0))
                {
                    tmpImage3[i*inputImage->getColumns() + j] = 255;
                }
                else if((i + 1 < inputImage->getRows())&&(j + 1 < inputImage->getColumns())&&(tmpImage[(i+1)*inputImage->getColumns() + (j+1)] != 0))
                {
                    tmpImage3[i*inputImage->getColumns() + j] = 255;
                }
                else if((i - 1 >= 0)&&(tmpImage[(i-1)*inputImage->getColumns() + j] != 0))
                {
                    tmpImage3[i*inputImage->getColumns() + j] = 255;
                }
                else if((i + 1 < inputImage->getRows())&&(tmpImage[(i+1)*inputImage->getColumns() + j] != 0))
                {
                    tmpImage3[i*inputImage->getColumns() + j] = 255;
                }
                else if((j - 1 >= 0)&&(tmpImage[i*inputImage->getColumns() + (j-1)] == 0))
                {
                    tmpImage3[i*inputImage->getColumns() + j] = 255;
                }
                else if((j + 1 < inputImage->getColumns())&&(tmpImage[i*inputImage->getColumns() + (j+1)] != 0))
                {
                    tmpImage3[i*inputImage->getColumns() + j] = 255;
                }
                else
                {
                    tmpImage3[i*inputImage->getColumns() + j] = 255;
                }
            }
        }
    }

    arr = tmpImage;
    tmpImage = tmpImage3;
    delete [] arr;
}

Glib::RefPtr<Gdk::Pixbuf> CalcificationDetector::createPixbuf()
{
    int columns = inputImage->getColumns();
    int rows = inputImage->getRows();
    Glib::RefPtr<Gdk::Pixbuf> image = Gdk::Pixbuf::create(Gdk::Colorspace::COLORSPACE_RGB, true, 8, columns, rows);
    int rstr = image->get_rowstride();
    int nchn = image->get_n_channels();
    guchar *px = image->get_pixels();

    int k = 0;
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < columns; j++)
        {
            guchar *p = px + i *rstr + j * nchn;
            p[2] = tmpImage[k];
            if(tmpImage[k] != 0)
            p[3] = 255;
            else
                p[3] = 0;

            k++;
        }

    }
    return image;
}
