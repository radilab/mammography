#include "GlandularTissueDetector.h"

GlandularTissueDetector::GlandularTissueDetector(RawImage *img)
{
    this->inputImage = img;
}

GlandularTissueDetector::~GlandularTissueDetector()
{
    //dtor
}

void GlandularTissueDetector::detect()
{
    double avg = 0;
    int pixelCount = 0;
    uint8_t *dataTmp = new uint8_t[inputImage->getRows() * inputImage->getColumns()];
    uint8_t *data = new uint8_t[inputImage->getRows() * inputImage->getColumns()];
    for(int i = 0; i < inputImage->getRows() * inputImage->getColumns(); i++)
    {
        if(inputImage->getPixelData()[i] != 0)
        {
            dataTmp[i] = inputImage->getPixelData()[i];
        }
    }

    //Smooth the image
    //Gauss kernel
    double factor = 500.0;
    double kernel[5][5] = {{2/factor, 4/factor, 5/factor, 4/factor, 2/factor},
                         {4/factor, 9/factor, 12/factor, 9/factor, 4/factor},
                         {5/factor, 12/factor, 15/factor, 12/factor, 5/factor},
                         {4/factor, 9/factor, 12/factor, 9/factor, 4/factor},
                         {2/169.0, 4/factor, 5/factor, 4/factor, 2/factor}};

    for(int i = 0; i < inputImage->getRows(); i++)
    {
        for(int j = 0; j < inputImage->getColumns(); j++)
        {
            int sum = 0;
            for(int k = -2; k < 3; k++)
            {
                for(int l = -2; l < 3; l++)
                {
                    if(((i + k) >= 0)&&((i + k) < inputImage->getRows())&&((j + l) >= 0)&&((j + l) < inputImage->getColumns()))
                    {
                        sum += dataTmp[(i+k)*inputImage->getColumns() + j+l] * kernel[2+k][2+l];
                    }
                }
            }
            data[i*inputImage->getColumns() + j] = sum;
        }
    }
    delete [] dataTmp;

    for(int i = 0; i < inputImage->getRows() * inputImage->getColumns(); i++)
    {
        if(data[i] != 0)
        {
            pixelCount++;
            avg += data[i];
        }
    }

    avg = avg / pixelCount;

    percent = 0;
    area = 0;
    breast = new RawImage(inputImage->getRows(), inputImage->getColumns());

    for(int i = 0; i < inputImage->getRows() * inputImage->getColumns(); i++)
    {
        if(data[i] >= avg)
        {
            breast->getPixelData()[i] = inputImage->getPixelData()[i];
            area++;
            percent++;
        }
        else
        {
            breast->getPixelData()[i] = 0;
        }
    }

    percent = percent / pixelCount;

    delete [] data;
}

Glib::RefPtr<Gdk::Pixbuf> GlandularTissueDetector::createPixbuf()
{
    int columns = breast->getColumns();
    int rows = breast->getRows();
    Glib::RefPtr<Gdk::Pixbuf> image = Gdk::Pixbuf::create(Gdk::Colorspace::COLORSPACE_RGB, true, 8, columns, rows);
    int rstr = image->get_rowstride();
    int nchn = image->get_n_channels();
    guchar *px = image->get_pixels();

    int k = 0;
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < columns; j++)
        {
            guchar *p = px + i *rstr + j * nchn;
            if((guchar)breast->getPixelData()[k] != 0)
            {
                p[0] = 255;
                p[1] = 255;
                p[2] = 255;
                p[3] = 80;
            }
            else
            {
                p[0] = 0;
                p[1] = 0;
                p[2] = 0;
                p[3] = 0;
            }
            k++;
        }

    }
    return image;
}
