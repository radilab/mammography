#ifndef BREASTDETECTOR_H
#define BREASTDETECTOR_H

#include <vector>
#include <stack>
#include <gtkmm.h>

#include "../image/rawimage.h"
#include "PixelState.h"

class BreastDetector
{
    public:
        BreastDetector(RawImage *img);
        void detect();
        Glib::RefPtr<Gdk::Pixbuf> createPixbuf();
        RawImage *getBreast();
        virtual ~BreastDetector();

    protected:
        RawImage *inputImage;
        PixelState **states;
        RawImage *breast;

    private:
};

#endif // BREASTDETECTOR_H
