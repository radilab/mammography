#ifndef PECTORALMUSCLEDETECTOR_H
#define PECTORALMUSCLEDETECTOR_H

#define _USE_MATH_DEFINES

#include <cmath>
#include <gtkmm.h>
#include "../image/rawimage.h"


class PectoralMuscleDetector
{
    public:
        PectoralMuscleDetector(RawImage *img, bool isLeft);
        void detect();
        Glib::RefPtr<Gdk::Pixbuf> createPixbuf();
        RawImage *getBreast();
        int getRoh() {return this->roh;};
        int getTheta() {return this->theta;};
        virtual ~PectoralMuscleDetector();

    protected:
        RawImage *inputImage;
        RawImage *breast;

    private:
        int roh;
        int theta;
        bool left;
};

#endif // PECTORALMUSCLEDETECTOR_H
