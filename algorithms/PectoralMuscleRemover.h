#ifndef PECTORALMUSCLEREMOVER_H
#define PECTORALMUSCLEREMOVER_H

#include <gtkmm.h>
#include "../image/rawimage.h"

class PectoralMuscleRemover
{
    public:
        PectoralMuscleRemover(RawImage *img, double x1, double y1, double x2, double y2, bool isLeft);
        void detect();
        Glib::RefPtr<Gdk::Pixbuf> createPixbuf();
        RawImage *getBreast() {return breast;};
        virtual ~PectoralMuscleRemover();

    protected:
        RawImage *inputImage;
        RawImage *breast;

    private:
        double x1;
        double x2;
        double y1;
        double y2;
        bool left;
};

#endif // PECTORALMUSCLEREMOVER_H
