#include "PectoralMuscleDetector.h"

PectoralMuscleDetector::PectoralMuscleDetector(RawImage *img, bool isLeft)
{
    inputImage = img;
    left = isLeft;
}

PectoralMuscleDetector::~PectoralMuscleDetector()
{
    //dtor
}

void PectoralMuscleDetector::detect()
{
    double avg = 0;
    int pixelCount = 0;
    uint8_t *dataTmp = new uint8_t[inputImage->getRows() * inputImage->getColumns()];
    uint8_t *data = new uint8_t[inputImage->getRows() * inputImage->getColumns()];
    for(int i = 0; i < inputImage->getRows() * inputImage->getColumns(); i++)
    {
        if(inputImage->getPixelData()[i] != 0)
        {
            dataTmp[i] = inputImage->getPixelData()[i];
        }
    }

    //Smooth the image
    //Gauss kernel
    double factor = 500.0;
    double kernel[5][5] = {{2/factor, 4/factor, 5/factor, 4/factor, 2/factor},
                         {4/factor, 9/factor, 12/factor, 9/factor, 4/factor},
                         {5/factor, 12/factor, 15/factor, 12/factor, 5/factor},
                         {4/factor, 9/factor, 12/factor, 9/factor, 4/factor},
                         {2/169.0, 4/factor, 5/factor, 4/factor, 2/factor}};

    for(int i = 0; i < inputImage->getRows(); i++)
    {
        for(int j = 0; j < inputImage->getColumns(); j++)
        {
            int sum = 0;
            for(int k = -2; k < 3; k++)
            {
                for(int l = -2; l < 3; l++)
                {
                    if(((i + k) >= 0)&&((i + k) < inputImage->getRows())&&((j + l) >= 0)&&((j + l) < inputImage->getColumns()))
                    {
                        sum += dataTmp[(i+k)*inputImage->getColumns() + j+l] * kernel[2+k][2+l];
                    }
                }
            }
            data[i*inputImage->getColumns() + j] = sum;
        }
    }
    delete [] dataTmp;

    for(int i = 0; i < inputImage->getRows() * inputImage->getColumns(); i++)
    {
        if(data[i] != 0)
        {
            pixelCount++;
            avg += data[i];
        }
    }

    avg = avg / pixelCount;

    breast = new RawImage(inputImage->getRows(), inputImage->getColumns());

    for(int i = 0; i < inputImage->getRows() * inputImage->getColumns(); i++)
    {
        if(data[i] >= avg)
        {
            data[i] = 1;
        }
        else
        {
            data[i] = 0;
        }
    }

    int total_pixels = 0;
    for(int i = 0; i < inputImage->getRows(); i++)
    {
        for(int j = 0; j < inputImage->getColumns(); j++)
        {
            if(data[i*inputImage->getColumns() + j] == 1)
            {
                if((i - 1 >= 0)&&(j - 1 >=0)&&(data[(i-1)*inputImage->getColumns() + (j-1)] == 0))
                {
                    data[i*inputImage->getColumns() + j] = 255;
                }
                else if((i + 1 < inputImage->getRows())&&(j - 1 >=0)&&(data[(i+1)*inputImage->getColumns() + (j-1)] == 0))
                {
                    data[i*inputImage->getColumns() + j] = 255;
                }
                else if((i - 1 >= 0)&&(j + 1 < inputImage->getColumns())&&(data[(i-1)*inputImage->getColumns() + (j+1)] == 0))
                {
                    data[i*inputImage->getColumns() + j] = 255;
                }
                else if((i + 1 < inputImage->getRows())&&(j + 1 < inputImage->getColumns())&&(data[(i+1)*inputImage->getColumns() + (j+1)] == 0))
                {
                    data[i*inputImage->getColumns() + j] = 255;
                }
                else if((i - 1 >= 0)&&(data[(i-1)*inputImage->getColumns() + j] == 0))
                {
                    data[i*inputImage->getColumns() + j] = 255;
                }
                else if((i + 1 < inputImage->getRows())&&(data[(i+1)*inputImage->getColumns() + j] == 0))
                {
                    data[i*inputImage->getColumns() + j] = 255;
                }
                else if((j - 1 >= 0)&&(data[i*inputImage->getColumns() + (j-1)] == 0))
                {
                    data[i*inputImage->getColumns() + j] = 255;
                }
                else if((j + 1 < inputImage->getColumns())&&(data[i*inputImage->getColumns() + (j+1)] == 0))
                {
                    data[i*inputImage->getColumns() + j] = 255;
                }
            }

            if(data[i*inputImage->getColumns() + j] == 255)
            {
                breast->getPixelData()[i*inputImage->getColumns() + j] = data[i*inputImage->getColumns() + j];
                total_pixels++;
            }
            else
            {
                breast->getPixelData()[i*inputImage->getColumns() + j] = 0;
            }
        }
    }

    int diagonal = ceil(sqrt(inputImage->getColumns()*inputImage->getColumns() + inputImage->getRows()*inputImage->getRows()));

    //Buckets of lines Roh - rows, Theta - columns
    int pixelAcc = 2;
    int totalRows = (diagonal/pixelAcc) * 2;
    int totalColumns = 180;
    int **buckets = new int*[totalRows];
    for(int i = 0; i < totalRows; i++)
    {
        buckets[i] = new int[totalColumns];
        for(int j = 0; j < totalColumns; j++)
        {
            buckets[i][j] = 0;
        }
    }

    //Calculate the lines
    for(int i = 0; i < inputImage->getRows() / 2; i++)
    {
        for(int j = 25; j < inputImage->getColumns() - 25; j++)
        {
            if(data[i * inputImage->getColumns() + j] == 255)
            {
                for(int k = 0; k < 181; k++)
                {
                    int roh = j * cos(k * M_PI/180) + i * sin(k * M_PI/180);
                    if(left && roh < 0 && k >= 90 && k <= 180)
                    {
                        buckets[totalRows/2 + roh/pixelAcc][k] += 1;
                    }
                    else if(!left && roh > 0 && k >= 0 && k <= 90)
                    {
                        buckets[totalRows/2 + roh/pixelAcc][k] += 1;
                    }

                }
            }
        }
    }
    //Find best line fit
    int maxRoh = 0;
    int maxTheta = 0;

    for(int i =0; i < totalRows; i++)
    {
        for(int j = 0; j < totalColumns; j++)
        {
            if(buckets[i][j] > buckets[maxRoh][maxTheta])
            {
                maxRoh = i;
                maxTheta = j;
            }

        }
    }


    this->roh = maxRoh*pixelAcc - (totalRows*pixelAcc) / 2;
    this->theta = maxTheta;

    delete [] data;
}

Glib::RefPtr<Gdk::Pixbuf> PectoralMuscleDetector::createPixbuf()
{
    int columns = breast->getColumns();
    int rows = breast->getRows();
    Glib::RefPtr<Gdk::Pixbuf> image = Gdk::Pixbuf::create(Gdk::Colorspace::COLORSPACE_RGB, true, 8, columns, rows);
    int rstr = image->get_rowstride();
    int nchn = image->get_n_channels();
    guchar *px = image->get_pixels();

    int k = 0;
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < columns; j++)
        {
            guchar *p = px + i *rstr + j * nchn;
            if((guchar)breast->getPixelData()[k] != 0)
            {
                p[0] = 0;
                p[1] = 255;
                p[2] = 0;
                p[3] = 255;
            }
            else
            {
                p[0] = 0;
                p[1] = 0;
                p[2] = 0;
                p[3] = 0;
            }
            k++;
        }

    }
    return image;
}

RawImage *PectoralMuscleDetector::getBreast()
{
    return breast;
}

