#ifndef ASYMMETRYDETECTOR_H
#define ASYMMETRYDETECTOR_H

#include <cmath>
#include <gtkmm.h>
#include "../image/rawimage.h"
#include "PixelState.h"

class AsymmetryDetector
{
    public:
        AsymmetryDetector(RawImage *imgL, RawImage *imageR);
        void detect();
        Glib::RefPtr<Gdk::Pixbuf> createPixbufL();
        Glib::RefPtr<Gdk::Pixbuf> createPixbufR();
        RawImage *getBreastL() {return breastL;};
        RawImage *getBreastR() {return breastR;};
        virtual ~AsymmetryDetector();
        void tumorDetect(RawImage *glandularTissue, bool isLeft);
        vector<vector<PixelState *>> tumorsL;
        vector<vector<PixelState *>> tumorsR;

    protected:
        RawImage *inputImageL;
        RawImage *inputImageR;
        RawImage *breastL;
        RawImage *breastR;

    private:
};

#endif // ASYMMETRYDETECTOR_H
