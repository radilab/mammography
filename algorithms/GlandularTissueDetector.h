#ifndef GLANDULARTISSUEDETECTOR_H
#define GLANDULARTISSUEDETECTOR_H

#include <gtkmm.h>
#include "../image/rawimage.h"

class GlandularTissueDetector
{
    public:
        GlandularTissueDetector(RawImage *img);
        void detect();
        Glib::RefPtr<Gdk::Pixbuf> createPixbuf();
        RawImage *getBreast() {return breast;};
        double getPercent() {return this->percent;};
        int getArea() {return this->area;};
        virtual ~GlandularTissueDetector();

    protected:
        RawImage *inputImage;
        RawImage *breast;

    private:
        double percent;
        int area;
};

#endif // GLANDULARTISSUEDETECTOR_H
