#ifndef CALCIFICATIONDETECTOR_H
#define CALCIFICATIONDETECTOR_H

#include <gtkmm.h>
#include "../image/rawimage.h"

class CalcificationDetector
{
    public:
        CalcificationDetector(RawImage *img);
        void detect();
        Glib::RefPtr<Gdk::Pixbuf> createPixbuf();
        RawImage *getBreast();
        virtual ~CalcificationDetector();

    protected:
        RawImage *inputImage;
        RawImage *breast;
        uint8_t *tmpImage;

    private:
};

#endif // CALCIFICATIONDETECTOR_H
