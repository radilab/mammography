#include "BreastDetector.h"

BreastDetector::BreastDetector(RawImage *img)
{
    inputImage = img;
}

BreastDetector::~BreastDetector()
{
    //dtor
}

void BreastDetector::detect()
{
    states = new PixelState*[inputImage->getRows() * inputImage->getColumns()];
    for(int i = 0, k = 0; i < inputImage->getRows(); i++)
    {
        for(int j = 0; j < inputImage->getColumns(); j++)
        {
            if(inputImage->getPixelData()[k] != 0)
            {
                PixelState *ps  = new PixelState();
                ps->x = j;
                ps->y = i;
                ps->state = 0;
                states[k] = ps;
            }
            else
            {
                states[k] = NULL;
            }
            k++;
        }
    }

    vector<vector<PixelState *>> segments;
    stack<PixelState *> toProcess;

    for(int i = 0; i < inputImage->getRows() * inputImage->getColumns(); i++)
    {
        if((states[i] != NULL)&&(states[i]->state == 0))
        {
            vector<PixelState *> segment;
            toProcess.push(states[i]);

            while(!toProcess.empty())
            {
                PixelState *currentState = toProcess.top();
                toProcess.pop();

                if((currentState != NULL)&&(currentState->state == 0))
                {
                    currentState->state = 1;
                    segment.push_back(currentState);

                    if(currentState->x - 1 >= 0)
                    {
                        toProcess.push(states[(currentState->y * inputImage->getColumns() + currentState->x - 1)]);
                    }
                    if(currentState->x + 1 < inputImage->getColumns())
                    {
                        toProcess.push(states[(currentState->y * inputImage->getColumns() + currentState->x + 1)]);
                    }
                    if(currentState->y - 1 >= 0)
                    {
                        toProcess.push(states[( (currentState->y - 1) * inputImage->getColumns() + currentState->x)]);
                    }
                    if(currentState->y + 1 < inputImage->getRows())
                    {
                        toProcess.push(states[( (currentState->y + 1) * inputImage->getColumns() + currentState->x)]);
                    }
                }
            }
            segments.push_back(segment);
        }
    }

    vector<PixelState *> *maxSegment = &segments[0];
    for(int i = 0; i < segments.size(); i++)
    {
        if(segments[i].size() > maxSegment->size())
        {
            maxSegment = &segments[i];
        }
    }

    breast = new RawImage(inputImage->getRows(), inputImage->getColumns());
    for(int i = 0; i < maxSegment->size(); i++)
    {
        int index = (*maxSegment)[i]->y * breast->getColumns() + (*maxSegment)[i]->x;
        breast->getPixelData()[index] = inputImage->getPixelData()[index];
    }


    //Free up everything
    for(int i = 0; i < inputImage->getRows() * inputImage->getColumns(); i++)
    {
        delete states[i];
    }
    delete [] states;
}

Glib::RefPtr<Gdk::Pixbuf> BreastDetector::createPixbuf()
{
    int columns = breast->getColumns();
    int rows = breast->getRows();
    Glib::RefPtr<Gdk::Pixbuf> image = Gdk::Pixbuf::create(Gdk::Colorspace::COLORSPACE_RGB, true, 8, columns, rows);
    int rstr = image->get_rowstride();
    int nchn = image->get_n_channels();
    guchar *px = image->get_pixels();

    int k = 0;
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < columns; j++)
        {
            guchar *p = px + i *rstr + j * nchn;
            if((guchar)breast->getPixelData()[k] != 0)
            {
                p[0] = 255;
                p[1] = 127;
                p[2] = 0;
                p[3] = 127;
            }
            else
            {
                p[0] = 0;
                p[1] = 0;
                p[2] = 0;
                p[3] = 0;
            }
            k++;
        }

    }
    return image;
}

RawImage *BreastDetector::getBreast()
{
    return breast;
}
