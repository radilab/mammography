#include "PectoralMuscleRemover.h"

PectoralMuscleRemover::PectoralMuscleRemover(RawImage *img, double x1, double y1, double x2, double y2, bool isLeft)
{
    this->inputImage = img;
    this->x1 = x1;
    this->x2 = x2;
    this->y1 = y1;
    this->y2 = y2;
    this->left = isLeft;
}

PectoralMuscleRemover::~PectoralMuscleRemover()
{
}

void PectoralMuscleRemover::detect()
{
    breast = new RawImage(inputImage->getRows(), inputImage->getColumns());
    for(int i = 0; i < inputImage->getRows(); i++)
    {
        for(int j = 0; j < inputImage->getColumns(); j++)
        {
            double result = (x2 - x1) * (i - y1) - (y2 - y1) * (j - x1);
            if((result > 0)&&(left))
            {
                breast->getPixelData()[i * inputImage->getColumns() + j] = inputImage->getPixelData()[i * inputImage->getColumns() + j];
            }
            else if((result > 0)&&(!left))
            {
                breast->getPixelData()[i * inputImage->getColumns() + j] = inputImage->getPixelData()[i * inputImage->getColumns() + j];
            }
            else
            {
                breast->getPixelData()[i * inputImage->getColumns() + j] = 0;
            }
        }
    }
}

Glib::RefPtr<Gdk::Pixbuf> PectoralMuscleRemover::createPixbuf()
{
    int columns = breast->getColumns();
    int rows = breast->getRows();
    Glib::RefPtr<Gdk::Pixbuf> image = Gdk::Pixbuf::create(Gdk::Colorspace::COLORSPACE_RGB, true, 8, columns, rows);
    int rstr = image->get_rowstride();
    int nchn = image->get_n_channels();
    guchar *px = image->get_pixels();

    int k = 0;
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < columns; j++)
        {
            guchar *p = px + i *rstr + j * nchn;
            if((guchar)breast->getPixelData()[k] != 0)
            {
                p[0] = 255;
                p[1] = 0;
                p[2] = 255;
                p[3] = 62;
            }
            else
            {
                p[0] = 0;
                p[1] = 0;
                p[2] = 0;
                p[3] = 0;
            }
            k++;
        }

    }
    return image;
}
