#include "AsymmetryDetector.h"

AsymmetryDetector::AsymmetryDetector(RawImage *imgL, RawImage *imgR)
{
    inputImageL = imgL;
    inputImageR = imgR;
}

AsymmetryDetector::~AsymmetryDetector()
{
}

void AsymmetryDetector::detect()
{
    breastL = new RawImage(inputImageL->getRows(), inputImageL->getColumns());
    breastR = new RawImage(inputImageL->getRows(), inputImageL->getColumns());
    for(int i = 0; i < inputImageL->getRows(); i++)
    {
        for(int j = 0; j < inputImageL->getColumns(); j++)
        {
            int diff = abs(inputImageL->getPixelData()[i * inputImageL->getColumns() + j] - inputImageR->getPixelData()[i * inputImageL->getColumns() + inputImageL->getColumns() - j]);
            if(diff == 0)
            {
                diff = 1;
            }

            if(inputImageL->getPixelData()[i * inputImageL->getColumns() + j] != 0)
            {
                breastL->getPixelData()[i * inputImageL->getColumns() + j] = diff;
            }
            else
            {
                breastL->getPixelData()[i * inputImageL->getColumns() + j] = 0;
            }

            if(inputImageR->getPixelData()[i * inputImageL->getColumns() + inputImageL->getColumns() - j] != 0)
            {
                breastR->getPixelData()[i * inputImageL->getColumns() + inputImageL->getColumns() - j] = diff;
            }
            else
            {
                breastR->getPixelData()[i * inputImageL->getColumns() + inputImageL->getColumns() - j] = 0;
            }
        }
    }

    tumorDetect(breastL, true);
    tumorDetect(breastR, false);
}

void AsymmetryDetector::tumorDetect(RawImage *glandularTissue, bool isLeft)
{
    //Segment the glandular tissue

    PixelState **statesL = new PixelState*[glandularTissue->getRows() * glandularTissue->getColumns()];
    for(int i = 0, k = 0; i < glandularTissue->getRows(); i++)
    {
        for(int j = 0; j < glandularTissue->getColumns(); j++)
        {
            if(glandularTissue->getPixelData()[k] != 0)
            {
                PixelState *ps  = new PixelState();
                ps->x = j;
                ps->y = i;
                ps->state = 0;
                statesL[k] = ps;
            }
            else
            {
                statesL[k] = NULL;
            }
            k++;
        }
    }

    vector<vector<PixelState *>> segments;
    stack<PixelState *> toProcess;

    for(int i = 0; i < glandularTissue->getRows() * glandularTissue->getColumns(); i++)
    {
        if((statesL[i] != NULL)&&(statesL[i]->state == 0))
        {
            vector<PixelState *> segment;
            toProcess.push(statesL[i]);

            while(!toProcess.empty())
            {
                PixelState *currentState = toProcess.top();
                toProcess.pop();

                if((currentState != NULL)&&(currentState->state == 0))
                {
                    currentState->state = 1;
                    segment.push_back(currentState);

                    if(currentState->x - 1 >= 0)
                    {
                        toProcess.push(statesL[(currentState->y * glandularTissue->getColumns() + currentState->x - 1)]);
                    }
                    if(currentState->x + 1 < glandularTissue->getColumns())
                    {
                        toProcess.push(statesL[(currentState->y * glandularTissue->getColumns() + currentState->x + 1)]);
                    }
                    if(currentState->y - 1 >= 0)
                    {
                        toProcess.push(statesL[( (currentState->y - 1) * glandularTissue->getColumns() + currentState->x)]);
                    }
                    if(currentState->y + 1 < glandularTissue->getRows())
                    {
                        toProcess.push(statesL[( (currentState->y + 1) * glandularTissue->getColumns() + currentState->x)]);
                    }
                }
            }
            segments.push_back(segment);
        }
    }

    //Find the tumors
    for(int i = 0; i < segments.size(); i++)
    {
        if(segments[i].size() > 10000)
        {
            double avg = 0;
            for(int j = 0; j < segments[i].size(); j++)
            {
                avg += glandularTissue->getPixelData()[segments[i][j]->y * glandularTissue->getColumns() + segments[i][j]->x];
            }

            avg = avg/segments[i].size();
            if(avg > 255*0.45)
            {
                if(isLeft)
                {
                    tumorsL.push_back(segments[i]);
                    for(int x = 0; x < segments[i].size(); x++)
                    {
                        breastL->getPixelData()[segments[i][x]->y * breastL->getColumns() + segments[i][x]->x] = 255;
                    }
                }
                else
                {
                    tumorsR.push_back(segments[i]);
                }

            }
        }
    }
}

Glib::RefPtr<Gdk::Pixbuf> AsymmetryDetector::createPixbufL()
{
    int columns = breastL->getColumns();
    int rows = breastL->getRows();
    Glib::RefPtr<Gdk::Pixbuf> image = Gdk::Pixbuf::create(Gdk::Colorspace::COLORSPACE_RGB, true, 8, columns, rows);
    int rstr = image->get_rowstride();
    int nchn = image->get_n_channels();
    guchar *px = image->get_pixels();

    int k = 0;
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < columns; j++)
        {
            guchar *p = px + i *rstr + j * nchn;
            if((guchar)breastL->getPixelData()[k] != 0)
            {
                p[0] = breastL->getPixelData()[k];
                p[1] = 255 - breastL->getPixelData()[k];
                p[2] = 0;
                p[3] = 255;
            }
            else
            {
                p[0] = 0;
                p[1] = 0;
                p[2] = 0;
                p[3] = 0;
            }
            k++;
        }

    }
    return image;
}

Glib::RefPtr<Gdk::Pixbuf> AsymmetryDetector::createPixbufR()
{
    int columns = breastR->getColumns();
    int rows = breastR->getRows();
    Glib::RefPtr<Gdk::Pixbuf> image = Gdk::Pixbuf::create(Gdk::Colorspace::COLORSPACE_RGB, true, 8, columns, rows);
    int rstr = image->get_rowstride();
    int nchn = image->get_n_channels();
    guchar *px = image->get_pixels();

    int k = 0;
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < columns; j++)
        {
            guchar *p = px + i *rstr + j * nchn;
            if((guchar)breastR->getPixelData()[k] != 0)
            {
                p[0] = breastR->getPixelData()[k];
                p[1] = 255 - breastR->getPixelData()[k];
                p[2] = 0;
                p[3] = 255;
            }
            else
            {
                p[0] = 0;
                p[1] = 0;
                p[2] = 0;
                p[3] = 0;
            }
            k++;
        }

    }
    return image;
}
